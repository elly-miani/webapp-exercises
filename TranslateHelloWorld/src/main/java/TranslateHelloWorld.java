package it.unipd.dei.webapp;
import java.io.BufferedReader; 
import java.io.InputStreamReader; 
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

import org.json.simple.JSONArray; 
import org.json.simple.JSONObject; 
import org.json.simple.JSONValue;

public class TranslateHelloWorld {

	/** The URL of the Web API for translation **/
	private static final String URL = "https://translate.yandex.net/api/v1.5/tr.json/translate?key=%s&lang=en-%s&text=Hello+world";

	public static void main(String[] args) throws Exception {

		// get the target language or use Italian as default one
		final String lang = (args.length > 0 ? args[0] : "it");
		
		// get the API key or use Italian as default one
		final String key = (args.length > 1 ? args[1] : "trnsl.1.1.XXXX");

		// create the URL for the connection
		URL url = new URL(String.format(URL, key, lang));

		// open the connection to the Web server and set a GET request
		final HttpsURLConnection c = (HttpsURLConnection) url.openConnection(); 
		c.setRequestMethod("GET");

		// read the response from the Web API and parse it into a JSON object
		final JSONObject json = (JSONObject) JSONValue
				.parse(new BufferedReader(new InputStreamReader(c.getInputStream())));
		
		// extract the array of strings contained in the "text" field and get the first string
		final String translation = (String) ((JSONArray) json.get("text")).get(0);

		// extract the value of the "text" field and print it to the screen
		System.out.printf("%n%s%n", translation);

		// close the connection
		c.disconnect();

	}
}
