<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html lang="en">

<head>
  <!-- Head tags and Bootstrap and Fontawesome css imports -->
  <c:import url="/jsp/include/head.jsp" />

  <!-- TODO: Remember to use "<c:url value="/folder/path"/>" for urls-->

  <!-- Custom Stylesheet -->
  <link rel="stylesheet" type="text/css" href="<c:url value="/css/styles.css"/>">

  <title> PAGE TITLE </title>

</head>

<body>
  <div class="container">
	<h1>My Toys Catalog</h1>
	<img id="img-logo" src="<c:url value="/media/plush-bunny.jpg"/>">
	</img>
	<nav>
		<div class="nav nav-tabs" id="nav-tab" role="tablist">
			<a class="nav-item nav-link active" id="nav-create-tab" data-toggle="tab" href="#nav-create" 
				role="tab" aria-controls="nav-create" aria-selected="true">
				Create
			</a>
			<a class="nav-item nav-link" id="nav-browse-tab" data-toggle="tab" href="#nav-browse" 
				role="tab" aria-controls="nav-browse" aria-selected="false">
				Browse
			</a>
			<a class="nav-item nav-link" id="nav-search-tab" data-toggle="tab" href="#nav-search" 
				role="tab" aria-controls="nav-search" aria-selected="false">
				Search
			</a>
		</div>
	</nav>
	</div>

	<div class="tab-content" id="nav-tabContent">
		<div class="tab-pane fade container show active" id="nav-create" role="tabpanel" aria-labelledby="nav-create-tab">
			<div>Work in progress.</div>
		</div>
		<div class="tab-pane fade container" id="nav-browse" role="tabpanel" aria-labelledby="nav-browse-tab">
			<div>Work in progress.</div>
		</div>

		<!-- Search tab -->
		<div class="tab-pane fade container" id="nav-search" role="tabpanel" aria-labelledby="nav-search-tab">
	
			<form id="form-searchByCategory" method="GET" action="http://dbstud.dei.unipd.it:8080/mytoys-server-1.00/search-product-by-category">
				<div class="form-group">
					<label for="form-select-searchByCategory">Search By Category</label>
					<div class="row">

						<div class="col-md-3">
						<select class="form-control" id="form-select-searchByCategory" name="category">
							<option>Animal</option>
							<option>Car</option>
							<option>Doll</option>
							<option>Lego</option>
							<option>Toy Soldier</option>
						</select>
						</div>
						<div class="col-md-3">
							<button type="submit" class="btn btn-primary"><i class="fas fa-search"></i> &nbsp; Search</button>
						</div>

					</div>
				</div>
			</form>

			<hr/>
			
			<div id="search-results">
				<table id="table-search-results" class="table table-hover hidden">
				  <thead>
					<tr>
					  <th scope="col">ID</th>
					  <th scope="col">Category</th>
					  <th scope="col">Name</th>
					  <th scope="col">Age Range</th>
					  <th scope="col">Price</th>
					</tr>
				  </thead>
				  <tbody></tbody>
				</table>

				<div id="error-message" class="hidden alert alert-danger"></div>
			</div>


		</div>
	</div>






  

  <!-- Javascript imports -->
  <c:import url="/jsp/include/foot.jsp" />

  <!-- Custom Javascript -->
  <script type="text/javascript" src="<c:url value="/js/script.js"/>"></script>

</body>

</html>
