package it.unipd.dei.webapp.servlet;

// import databases and resources
import it.unipd.dei.webapp.database.SearchProductByCategoryDatabase;
import it.unipd.dei.webapp.resource.Product;
import it.unipd.dei.webapp.resource.Message;
import it.unipd.dei.webapp.resource.ResourceList;


import java.io.IOException;
import java.io.OutputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;


public final class SearchProductByCategoryServlet extends AbstractDatabaseServlet {

	// EXAMPLE WITH STRING PARAMETER

	public void doGet(HttpServletRequest req, HttpServletResponse res)
			throws ServletException, IOException 
	{

		// set the MIME media type of the response
		res.setContentType("application/json; charset=utf-8");

		// get a stream to write the response
		OutputStream out = res.getOutputStream();

		try 
		{
		
			// retrieves the request parameter
			String category = req.getParameter("category");

			// creates a new object for accessing the database and searching the requested resource
			List<Product> productList = new SearchProductByCategoryDatabase(getDataSource().getConnection(), category)
					.searchProductByCategory();
			
			// write the list of result to the client as JSON
			new ResourceList<Product>(productList).toJSON(out);
			
		} 
		catch (SQLException ex) 
		{
			Message m = new Message("Cannot search for products: unexpected error while accessing the database.", 
									"E100", ex.getMessage());
				
			res.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
			m.toJSON(out);
		}

		// flush the output stream buffer
		out.flush();

		// close the output stream
		out.close();
	}


}

