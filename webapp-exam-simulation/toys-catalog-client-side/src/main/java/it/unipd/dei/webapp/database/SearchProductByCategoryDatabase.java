package it.unipd.dei.webapp.database;

import it.unipd.dei.webapp.resource.Product;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


public final class SearchProductByCategoryDatabase 
{ 

	// The SQL statement to be executed
	// Select statement
	private static final String STATEMENT = "SELECT id, category, name, ageRange, price" + " FROM MyToys.Product" + " WHERE category = ?::MyToys.Category";

	// The connection to the database
	private final Connection con;

	// The category of the Product
	private final String category;


	// Creates a new object for searching Product(s) by category
	public SearchProductByCategoryDatabase(final Connection con, final String category) 
	{
		this.con = con;
		this.category = category;
	}


	// Searches Product(s) by category
	public List<Product> searchProductByCategory() throws SQLException 
	{

		PreparedStatement pstmt = null;
		ResultSet rs = null;

		// the results of the search
		final List<Product> productList = new ArrayList<Product>();

		try 
		{
			pstmt = con.prepareStatement(STATEMENT);
			pstmt.setString(1, category);

			rs = pstmt.executeQuery();

			while (rs.next()) {
				productList.add(
					new Product(
								rs.getInt("id"), 
								rs.getString("category"), 
								rs.getString("name"), 
								rs.getString("ageRange"), 
								rs.getDouble("price")
								)
							);
			}
		} 
		finally 
		{
			if (rs != null) {
				rs.close();
			}

			if (pstmt != null) {
				pstmt.close();
			}

			con.close();
		}

		return productList;
	}
}
