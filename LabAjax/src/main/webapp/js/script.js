const course = document.getElementById("course-form");
const number = document.getElementById("number-form");
const email = document.getElementById("email-form");
const form = document.getElementById("unipd-form"); 
const error_course = course.nextElementSibling;
const error_number = number.nextElementSibling;
const error_email = email.nextElementSibling;

const ajax_response = document.getElementById("ajax-response");

var httpRequest;

// Regular expression to check whether the input is an email address
const emailRegExp = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;


course.addEventListener("input", function(event) {
	
	if (course.value !== "ICT" &&  course.value !== "Informatics") {
		course.classList.add("invalid");
		course.classList.remove("valid");
		error_course.innerHTML = "Incorrect course name. Choose between Informatics and ICT.";
	}
	else {
		course.classList.add("valid");
		course.classList.remove("invalid");
		error_course.innerHTML = "";
	}
})


number.addEventListener("input", function(event) {
	
	if (number.value < 0 || number.value > 5 ) {
		number.classList.add("invalid");
		number.classList.remove("valid");
		error_number.innerHTML = "Select a number b/w 0 and 5.";
	}
	else {
		number.classList.add("valid");
		number.classList.remove("invalid");
		error_number.innerHTML = "";
	}
})

email.addEventListener("input", function(event) {
	if(emailRegExp.test(email.value) && email.value.length > 0) {
		email.classList.add("valid");
		email.classList.remove("invalid");
		error_email.innerHTML = "";
	}
	else {
		email.classList.add("invalid");
		email.classList.remove("valid");
		error_email.innerHTML = "Insert a valid email address.";
	}
})


form.addEventListener("submit", function(event) { 
	console.log("Sumbit button works");
	event.preventDefault();
	
	httpRequest = new XMLHttpRequest();

	if (!httpRequest) {
		alert('Cannot create an XMLHTTP instance');
		return false;
	}

	httpRequest.onLoad = responseHandler;

	httpRequest.open('GET', 'http://www.example.org/', true);
	httpRequest.setRequestHeader("Access-Control-Allow-Origin", "*");
	httpRequest.send();
})


function responseHandler() {
	if(httpRequest.readyState === XMLHttpRequest.DONE) {
		if (httphttphttp.status == 200) {
			ajax_response.innerHTML = httpRequest.responseText;
		}
		else {
			alert("There was a problem with the request");
		}
	}
}
